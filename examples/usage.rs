use serde_derive::{Deserialize, Serialize};
use specs::{self, Builder, WorldExt};
use sphynx;
use std::marker::PhantomData;

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct Pos([f32; 3]);
impl specs::Component for Pos {
    type Storage = specs::FlaggedStorage<Self, specs::VecStorage<Self>>;
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct Color([f32; 4]);
impl specs::Component for Color {
    type Storage = specs::FlaggedStorage<Self, specs::VecStorage<Self>>;
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct TimeOfDay(f32);
#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct DeltaTime(f32);

// Automatically derive From<T> for ResPacket for each variant ResPacket::T(T)
sphynx::sum_type! {
    #[derive(Clone, Debug, Serialize, Deserialize)]
    enum ResPacket {
        DeltaTime(DeltaTime),
        TimeOfDay(TimeOfDay),
    }
}
impl sphynx::ResPacket for ResPacket {}
// Automatically derive From<T> for CompPacket for each variant CompPacket::T(T)
sphynx::sum_type! {
    #[derive(Clone, Debug, Serialize, Deserialize)]
    enum CompPacket {
        Pos(Pos),
        Color(Color),
    }
}
// Automatically derive From<T> for CompPhantom for each variant CompPhantom::T(PhantomData<T>)
sphynx::sum_type! {
    #[derive(Clone, Debug, Serialize, Deserialize)]
    enum CompPhantom {
        Pos(PhantomData<Pos>),
        Color(PhantomData<Color>),
    }
}
impl sphynx::CompPacket for CompPacket {
    type Phantom = CompPhantom;
}

fn main() {
    let mut world = sphynx::World::<CompPacket, ResPacket>::new(specs::World::new(), |world| {
        world.insert_synced(DeltaTime(0.5));
        world.insert_synced(TimeOfDay(350.0));
        world.register_synced::<Pos>();
        world.register_synced::<Color>();
    });

    let test_entity = world
        .create_entity_synced()
        .with(Pos([0.0; 3]))
        .with(Color([0.0; 4]))
        .build();

    println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    world.write_storage::<Color>().remove(test_entity).unwrap();

    println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    let state_package = world.gen_state_package();
    println!("World 1, State Package:\n{:?}", state_package);
    let mut world2 = sphynx::World::<CompPacket, ResPacket>::from_state_package(
        specs::World::new(),
        |world| {
            world.insert_synced(DeltaTime(0.0));
            world.insert_synced(TimeOfDay(0.0));
            world.register_synced::<Pos>();
            world.register_synced::<Color>();
        },
        state_package,
    );

    println!("World 2, State Package:\n{:?}", world2.gen_state_package());

    println!("World 2, Sync Package:\n{:?}", world2.next_sync_package());

    //world.delete_entity_synced(test_entity).unwrap();
    world.write_storage::<Pos>().remove(test_entity).unwrap();

    //println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    let sync_p = world.next_sync_package();
    println!("World 1, Sync Package:\n{:?}", sync_p);
    world2.sync_with_package(sync_p);

    println!("World 2, State Package:\n{:?}", world2.gen_state_package());
}
