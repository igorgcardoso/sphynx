mod uid;

// Reexports
pub use sum_type::sum_type;
pub use uid::{Uid, UidAllocator};

#[cfg(feature = "serde1")]
use serde::{de::DeserializeOwned, Deserialize, Serialize};
use specs::{saveload::MarkerAllocator, shred, Builder, Join};
use std::{
    any::TypeId,
    collections::{HashMap, HashSet},
    convert::{TryFrom, TryInto},
    fmt::Debug,
    marker::PhantomData,
};
use sum_type::InvalidType;

pub trait CompPacket: Clone + Debug + Send + 'static {
    #[cfg(not(feature = "serde1"))]
    type Phantom: Clone + Debug;
    #[cfg(feature = "serde1")]
    type Phantom: Clone + Debug + Serialize + DeserializeOwned;
}

pub trait ResPacket: Clone + Debug + Send + 'static {}

#[derive(Copy, Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub enum CompUpdateKind<P: CompPacket> {
    Inserted(P),
    Modified(P),
    Removed(P::Phantom),
}

#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct StatePackage<P: CompPacket, R: ResPacket> {
    spectrum: u16,
    entities: HashMap<u64, Vec<P>>,
    resources: Vec<R>,
}

impl<P: CompPacket, R: ResPacket> Default for StatePackage<P, R> {
    fn default() -> Self {
        Self {
            spectrum: 0,
            entities: HashMap::new(),
            resources: Vec::new(),
        }
    }
}

#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct SyncPackage<P: CompPacket, R: ResPacket> {
    comp_updates: Vec<(u64, CompUpdateKind<P>)>,
    created_entities: HashSet<u64>,
    deleted_entities: HashSet<u64>,
    resources: Vec<R>,
}

pub struct World<P: CompPacket, R: ResPacket> {
    specs_world: specs::World,
    res_trackers: HashMap<TypeId, Box<dyn ResTracker<R>>>,
    trackers: HashMap<TypeId, Box<dyn Tracker<P>>>,
    created_entities: HashSet<u64>,
    deleted_entities: HashSet<u64>,
}

impl<P: CompPacket, R: ResPacket> std::ops::Deref for World<P, R> {
    type Target = specs::World;
    fn deref(&self) -> &specs::World {
        &self.specs_world
    }
}

impl<P: CompPacket, R: ResPacket> std::ops::DerefMut for World<P, R> {
    fn deref_mut(&mut self) -> &mut specs::World {
        &mut self.specs_world
    }
}

impl<P: CompPacket, R: ResPacket> World<P, R> {
    #[allow(dead_code)]
    pub fn new<F: FnOnce(&mut Self)>(specs_world: specs::World, register_fn: F) -> Self {
        Self::from_state_package(specs_world, register_fn, StatePackage::default())
    }

    #[allow(dead_code)]
    pub fn from_state_package<F: FnOnce(&mut Self)>(
        mut specs_world: specs::World,
        register_fn: F,
        state_package: StatePackage<P, R>,
    ) -> Self {
        specs_world.register::<uid::Uid>();
        specs_world.add_resource(uid::UidAllocator::new(state_package.spectrum));

        let mut this = Self {
            specs_world,
            res_trackers: HashMap::new(),
            trackers: HashMap::new(),
            created_entities: HashSet::new(),
            deleted_entities: HashSet::new(),
        };

        // Call a function to register types
        register_fn(&mut this);

        // Apply state package resources
        for res_packet in state_package.resources {
            this.res_trackers.values().for_each(|tracker| {
                tracker.try_update_packet(&this.specs_world, res_packet.clone())
            });
        }

        // Apply state package entities
        for (entity_uid, packets) in state_package.entities {
            let entity = this.create_entity_with_uid(entity_uid);
            for packet in packets {
                this.trackers.values().for_each(|tracker| {
                    tracker.try_insert_packet(&this.specs_world, entity, packet.clone())
                });
            }
        }

        this.specs_world.maintain();

        // Clear change trackers to avoid echoing changes
        this.clear_tracker_events();
        this
    }

    fn clear_tracker_events(&mut self) {
        self.created_entities.clear();
        self.deleted_entities.clear();
        for tracker in self.trackers.values_mut() {
            tracker.clear_events(&self.specs_world);
        }
    }

    #[allow(dead_code)]
    pub fn register_synced<C: specs::Component + Clone + Send + Sync>(&mut self)
    where
        P: From<C>,
        C: TryFrom<P, Error = InvalidType>,
        P::Phantom: From<PhantomData<C>>,
        P::Phantom: TryInto<PhantomData<C>>,
        C::Storage: Default + specs::storage::Tracked,
    {
        self.specs_world.register::<C>();

        self.trackers.insert(
            TypeId::of::<C>(),
            Box::new(UpdateTracker::<C> {
                reader_id: self.specs_world.write_storage::<C>().register_reader(),
                inserted: specs::BitSet::new(),
                modified: specs::BitSet::new(),
                removed: specs::BitSet::new(),
                phantom: PhantomData,
            }),
        );
    }

    #[allow(dead_code)]
    pub fn insert_synced<C: shred::Resource + Clone + Send + Sync>(&mut self, res: C)
    where
        R: From<C>,
        C: TryFrom<R>,
    {
        self.specs_world.add_resource::<C>(res);

        self.res_trackers.insert(
            TypeId::of::<C>(),
            Box::new(ResUpdateTracker::<C> {
                phantom: PhantomData,
            }),
        );
    }

    #[allow(dead_code)]
    pub fn create_entity_synced(&mut self) -> specs::EntityBuilder {
        let entity_builder = self.specs_world.create_entity();
        let uid = entity_builder
            .world
            .write_resource::<uid::UidAllocator>()
            .allocate(entity_builder.entity, None);
        self.created_entities.insert(uid.into());
        entity_builder.with(uid)
    }

    #[allow(dead_code)]
    pub fn delete_entity_synced(
        &mut self,
        entity: specs::Entity,
    ) -> Result<(), specs::error::WrongGeneration> {
        let uid = self
            .specs_world
            .read_storage::<uid::Uid>()
            .get(entity)
            .cloned();

        if let Some(uid) = uid {
            if !self.created_entities.remove(&uid.into()) {
                self.deleted_entities.insert(uid.into());
            }
            self.specs_world.delete_entity(entity)
        } else {
            Ok(())
        }
    }

    /// Get the UID of an entity
    pub fn uid_from_entity(&self, entity: specs::Entity) -> Option<u64> {
        self.specs_world
            .read_storage::<uid::Uid>()
            .get(entity)
            .cloned()
            .map(|uid| uid.into())
    }

    /// Get the UID of an entity
    pub fn entity_from_uid(&self, uid: u64) -> Option<specs::Entity> {
        self.specs_world
            .read_resource::<uid::UidAllocator>()
            .retrieve_entity_internal(uid)
    }

    #[allow(dead_code)]
    pub fn gen_state_package(&self) -> StatePackage<P, R> {
        StatePackage {
            spectrum: self
                .specs_world
                .write_resource::<uid::UidAllocator>()
                .next_spectrum(),
            entities: (
                &self.specs_world.entities(),
                &self.specs_world.read_storage::<uid::Uid>(),
            )
                .join()
                .map(|(entity, &entity_uid)| {
                    let mut packets = Vec::new();
                    for tracker in self.trackers.values() {
                        tracker.add_packet_for(&self.specs_world, entity, &mut packets);
                    }
                    (entity_uid.into(), packets)
                })
                .collect(),
            resources: self
                .res_trackers
                .values()
                .map(|tracker| tracker.create_packet(&self.specs_world))
                .collect(),
        }
    }

    fn create_entity_with_uid(&mut self, entity_uid: u64) -> specs::Entity {
        let existing_entity = self
            .specs_world
            .read_resource::<uid::UidAllocator>()
            .retrieve_entity_internal(entity_uid.into());

        match existing_entity {
            Some(entity) => entity,
            None => {
                let entity_builder = self.specs_world.create_entity();
                let uid = entity_builder
                    .world
                    .write_resource::<uid::UidAllocator>()
                    .allocate(entity_builder.entity, Some(entity_uid));
                entity_builder.with(uid).build()
            }
        }
    }

    #[allow(dead_code)]
    pub fn sync_with_package(&mut self, package: SyncPackage<P, R>) {
        // Attempt to create entities
        for entity_uid in package.created_entities.into_iter() {
            self.create_entity_with_uid(entity_uid);
        }

        // Update resources
        for res_packet in package.resources {
            self.res_trackers.values().for_each(|tracker| {
                tracker.try_update_packet(&self.specs_world, res_packet.clone())
            });
        }

        // Update components
        for (entity_uid, update) in package.comp_updates.into_iter() {
            if let Some(entity) = self
                .specs_world
                .read_resource::<uid::UidAllocator>()
                .retrieve_entity_internal(entity_uid)
            {
                match update {
                    CompUpdateKind::Inserted(packet) => {
                        self.trackers.values().for_each(|tracker| {
                            tracker.try_insert_packet(&self.specs_world, entity, packet.clone())
                        });
                    }
                    CompUpdateKind::Modified(packet) => {
                        self.trackers.values().for_each(|tracker| {
                            tracker.try_modify_packet(&self.specs_world, entity, packet.clone())
                        });
                    }
                    CompUpdateKind::Removed(phantom) => {
                        self.trackers.values().for_each(|tracker| {
                            tracker.try_remove_packet(&self.specs_world, entity, phantom.clone())
                        });
                    }
                }
            }
        }

        // Attempt to delete entities that were marked for deletion
        for entity_uid in package.deleted_entities.into_iter() {
            let entity = self
                .specs_world
                .read_resource::<uid::UidAllocator>()
                .retrieve_entity_internal(entity_uid);
            if let Some(entity) = entity {
                let _ = self.specs_world.delete_entity(entity);
            }
        }

        // Clear tracker events to avoid an echo chamber
        self.clear_tracker_events();
    }

    #[allow(dead_code)]
    pub fn next_sync_package(&mut self) -> SyncPackage<P, R> {
        // Collect resource packets
        let mut resources = Vec::new();
        for tracker in self.res_trackers.values_mut() {
            resources.push(tracker.create_packet(&self.specs_world));
        }

        // Collect changed entity updates
        let mut comp_updates = Vec::new();
        for tracker in self.trackers.values_mut() {
            tracker.get_updates_of(&self.specs_world, &mut comp_updates);
        }

        // Swap out created / deleted entities
        let (mut created_entities, mut deleted_entities) = (HashSet::new(), HashSet::new());
        std::mem::swap(&mut created_entities, &mut self.created_entities);
        std::mem::swap(&mut deleted_entities, &mut self.deleted_entities);

        SyncPackage {
            comp_updates,
            created_entities,
            deleted_entities,
            resources,
        }
    }
}

pub trait ResTracker<R: ResPacket>: Send + 'static {
    fn create_packet(&self, specs_world: &specs::World) -> R;
    fn try_update_packet(&self, specs_world: &specs::World, packet: R);
}

struct ResUpdateTracker<R: shred::Resource> {
    phantom: PhantomData<R>,
}

impl<C: shred::Resource + Clone + Send + Sync, R: ResPacket> ResTracker<R> for ResUpdateTracker<C>
where
    R: From<C>,
    C: TryFrom<R>,
{
    fn create_packet(&self, specs_world: &specs::World) -> R {
        R::from((*specs_world.read_resource::<C>()).clone())
    }

    fn try_update_packet(&self, specs_world: &specs::World, packet: R) {
        if let Ok(res) = packet.try_into() {
            *specs_world.write_resource::<C>() = res;
        }
    }
}

pub trait Tracker<P: CompPacket>: Send + 'static {
    fn add_packet_for(
        &self,
        specs_world: &specs::World,
        entity: specs::Entity,
        packets: &mut Vec<P>,
    );
    fn try_insert_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P);
    fn try_modify_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P);
    fn try_remove_packet(
        &self,
        specs_world: &specs::World,
        entity: specs::Entity,
        packet: P::Phantom,
    );
    fn clear_events(&mut self, specs_world: &specs::World);
    fn get_updates_of(&mut self, world: &specs::World, out: &mut Vec<(u64, CompUpdateKind<P>)>);
}

struct UpdateTracker<C: specs::Component> {
    reader_id: specs::ReaderId<specs::storage::ComponentEvent>,
    inserted: specs::BitSet,
    modified: specs::BitSet,
    removed: specs::BitSet,
    phantom: PhantomData<C>,
}

impl<C: specs::Component + Clone + Send + Sync, P: CompPacket> Tracker<P> for UpdateTracker<C>
where
    P: From<C>,
    C: TryFrom<P>,
    P::Phantom: From<PhantomData<C>>,
    P::Phantom: TryInto<PhantomData<C>>,
    C::Storage: specs::storage::Tracked,
{
    fn add_packet_for(
        &self,
        specs_world: &specs::World,
        entity: specs::Entity,
        packets: &mut Vec<P>,
    ) {
        if let Some(comp) = specs_world.read_storage::<C>().get(entity) {
            packets.push(P::from(comp.clone()));
        }
    }

    fn try_insert_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P) {
        if let Ok(comp) = packet.try_into() {
            let _ = specs_world.write_storage::<C>().insert(entity, comp);
        }
    }

    fn try_modify_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P) {
        if let Ok(comp) = packet.try_into() {
            let _ = specs_world
                .write_storage::<C>()
                .get_mut(entity)
                .map(|c| *c = comp);
        }
    }

    fn try_remove_packet(
        &self,
        specs_world: &specs::World,
        entity: specs::Entity,
        phantom: P::Phantom,
    ) {
        if let Ok(_) = phantom.try_into() {
            let _ = specs_world.write_storage::<C>().remove(entity);
        }
    }

    fn clear_events(&mut self, specs_world: &specs::World) {
        // Consume all events
        specs_world
            .write_storage::<C>()
            .channel()
            .read(&mut self.reader_id)
            .for_each(|_| {});
    }

    fn get_updates_of(
        &mut self,
        specs_world: &specs::World,
        buf: &mut Vec<(u64, CompUpdateKind<P>)>,
    ) {
        self.modified.clear();
        self.inserted.clear();
        self.removed.clear();

        for event in specs_world
            .write_storage::<C>()
            .channel()
            .read(&mut self.reader_id)
        {
            match event {
                specs::storage::ComponentEvent::Inserted(id) => self.inserted.add(*id),
                specs::storage::ComponentEvent::Modified(id) => self.modified.add(*id),
                specs::storage::ComponentEvent::Removed(id) => self.removed.add(*id),
            };
        }

        // Generate inserted updates
        for (uid, comp, _) in (
            &specs_world.read_storage::<uid::Uid>(),
            &specs_world.read_storage::<C>(),
            &self.inserted,
        )
            .join()
        {
            buf.push((
                (*uid).into(),
                CompUpdateKind::Inserted(P::from(comp.clone())),
            ));
        }

        // Generate modified updates
        for (uid, comp, _) in (
            &specs_world.read_storage::<uid::Uid>(),
            &specs_world.read_storage::<C>(),
            &self.modified,
        )
            .join()
        {
            buf.push((
                (*uid).into(),
                CompUpdateKind::Modified(P::from(comp.clone())),
            ));
        }

        // Generate removed updates
        for (uid, _) in (&specs_world.read_storage::<uid::Uid>(), &self.removed).join() {
            buf.push((
                (*uid).into(),
                CompUpdateKind::Removed(P::Phantom::from(PhantomData::<C>)),
            ));
        }
    }
}
